/**
 * 20190818.1
 * Lost yesterday's changes
 * Add version
 * Print version to appear in standard output
 * Modify output of .txt and .err files
 */

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.Set;

import model.AminoAcid;
import model.Mutation;
import model.MutationKey;

import java.util.Hashtable;

public class FindMutations {
	static String version = "Bioj Version 1.20160818.1";

	public static void main(String[] args) {
		if (args.length==1) {
			if (args[0].equals("--version")) {
				// Version is compiled from release.date_compiled.comilation_for_the_day
				System.out.println(version);
			}
		} else if (args.length < 6) {
			System.out.println("Syntax: java -jar strand.txt MutationsRNA_MITOMAP.csv mt_genome.fast alignmentdir species output_filename_prefix");
		} else {
			System.out.println(version);
			String aafile = args[0];
			String mufile = args[1];
			String genfile = args[2];
			String speciesdir = args[3];
			String spec = args[4];
			String outputfilenamepref = args[5];
			Hashtable<String, AminoAcid> aminoacids = new Hashtable<String, AminoAcid>();

			File aminoacid_file = new File(aafile);
			File mutations_file = new File(mufile);
			File genome_file = new File(genfile);
			String species_dir = speciesdir;
			String species = spec;

			String species_file = species_dir + "/" + species;
			try {
				PrintWriter pw_output = new PrintWriter(new FileWriter(outputfilenamepref + ".txt"));
				PrintWriter pw_err = new PrintWriter(new FileWriter(outputfilenamepref + ".err"));
				//PrintWriter pw_seq = new PrintWriter(new FileWriter(species + ".seq"));
				pw_err.write("line\tname\tstart\tmutation\tend\trel\tpos\tallele\tlength\n");
				// Read mitochondrial genome into an array of char
				Scanner readmt = new Scanner(genome_file);
				@SuppressWarnings("unused")
				String header = readmt.nextLine();
				char[] genome = readmt.nextLine().toCharArray();
				readmt.close();

				// Read the list of aminoacids, their starting position on the genome and the strand
				Scanner reader = new Scanner(aminoacid_file);
				while (reader.hasNextLine()) {
					String line = reader.nextLine().trim();
					String[] tokens = line.split("\t");
					AminoAcid aminoacid = new AminoAcid(tokens[0], Integer.valueOf(tokens[1]), tokens[2].charAt(0), Integer.valueOf(tokens[3]));
					aminoacids.put(tokens[0], aminoacid);
				}
				reader.close();

				// Read the mutation from the file and add it to the appropriate amino acid
				Scanner mmread = new Scanner(mutations_file);
				header = mmread.nextLine();
				while (mmread.hasNextLine()) {
					String line = mmread.nextLine().trim();
					String[] token = line.split(",");
					if (token[4].startsWith("tRNA")) {
						String aa = token[4].substring(5);
						// Remove characters from amino acid string to adhere to format used in strand file
						aa = aa.replace(" (", "_");
						aa = aa.replace(")", "");
						aa = aa.replace("(", "_");
						aa = aa.replace(" ", "_");
						aa = aa.replace("_precursor", "");
						// get the appropriate amino acid from the hashtable
						AminoAcid aminoacid = aminoacids.get(aa);
						// calculate relative mutation position
						int position = Integer.valueOf(token[0]);
						int rel_position = 0;
						if (aminoacid.getStrand() == '+') {
							rel_position = position - aminoacid.getPosition();
						} else {
							rel_position = aminoacid.getPosition() - position;
						}
						Mutation mutation = new Mutation(position, token[1], token[2], token[3],
								token[4], token[5], token[6], token[7], Integer.valueOf(token[8]),
								Integer.valueOf(token[9]), rel_position);
						MutationKey mkey = new MutationKey(Integer.valueOf(token[0]), token[3]);
						// add this mutation to the amino acid
						aminoacid.addMutation(mkey, mutation);
					}
				}
				mmread.close();
				Set<String> keys = aminoacids.keySet();
				// for each amino acid
				for (String key : keys) {
					String ssequence = "";
					String line = "";
					AminoAcid aa = aminoacids.get(key);
					File filename = new File(species_file + "/tRNA-" + aa.getName() + ".mafft.txt");
					Scanner readsp = new Scanner(filename); // read amino acid file
					header = readsp.nextLine(); // 1st line in file, read the header for human
					String human = readsp.nextLine(); // 2nd line in file, read the human sequence
					while (readsp.hasNext()) { // in case the sequence stretches over more than one line
						line = readsp.nextLine(); // read the line
						if (line.startsWith(">")) { // if it starts with > it is a header line, break out of loop
							header = line;
							break;
						} else {
							ssequence = ssequence + line; // if it is a sequence, add it to the sequence
						}
					}
					Hashtable<MutationKey, Mutation> mutations = aa.getMutations();
					Set<MutationKey> mkeys = mutations.keySet();
					int number_of_mutations = mutations.size(); // the number of mutations for this amino acid
					int[] mutationcount = new int[number_of_mutations];

					// Read each line in amino acid file
					int number_of_sequences = 0; // number of sequences in the file
					while (readsp.hasNext()) {
						while (readsp.hasNext()) {
							line = readsp.nextLine(); // read next line
							if (line.startsWith(">")) { // if it starts with > it is a header
								header = line;
								break;
							} else {
								ssequence = ssequence + line;
							}
						}

						number_of_sequences++;
						char[] sequence = ssequence.toCharArray();
						ssequence = "";
						// for each mutation check whether this sequence contains it
						// if (aa.getName().startsWith("Leu_UUR")) pw_seq.println(sequence);
						for (MutationKey mkey : mkeys) {
							Mutation mutation = mutations.get(mkey);
							if (sequence.length > mutation.getRelative_position() && mutation.getRelative_position() > -1) {
								// get the mutated nucleotide and check against nucleotide in sequence
								// if equal, increment incCount_positive
								if (mutation.getMutation() == sequence[mutation.getRelative_position()]) {
									mutation.incCount_positive();
								}
							} else {
								int end;
								if (aa.getStrand() == '+') {
									end = (aa.getPosition() + aa.getLength());
								} else {
									end = (aa.getPosition() - aa.getLength());
								}
								pw_err.write(number_of_sequences
										+ "\t" + aa.getName()
										+ "\t" + aa.getPosition()
										+ "\t" + mutation.getPosition()
										+ "\t" + end
										+ "\t" + mutation.getRelative_position()
										+ "\t" + mutation.getPosition()
										+ "\t" + mutation.getAllele()
										+ "\t" + sequence.length
										+ "\n");

							}
						}
					}
					//Iterate through mutations in mutations hashtable and calculate the percentage of mutated SNPS
					for (MutationKey mkey : mkeys) {
						Mutation mutation = mutations.get(mkey);
						int end;
						if (aa.getStrand() == '+') {
							end = (aa.getPosition() + aa.getLength());
						} else {
							end = (aa.getPosition() - aa.getLength());
						}
						pw_output.write(aa.getName()
								+ "\t" + mutation.getRelative_position()
								+ "\t" + aa.getPosition()
								+ "\t" + mutation.getPosition()
								+ "\t" + end
								+ "\t" + mutation.getAllele()
								+ "\t" + mutation.getCount_positive()
								+ "\t" + number_of_sequences
								+ "\t" + ((double) mutation.getCount_positive() / (double) number_of_sequences)
								+ "\n");
					}

				}
				//pw_seq.close();
				pw_err.close();
				pw_output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Done.");
		}
	}
}
