package bioj;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import model.GenBankRecord;
import model.Locus;
import model.Version;

public class MitoMain {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file = new File("/home/jannetta/workspace/BioJ/NC_012920.full.gb");
		GenBankRecord gbr = new GenBankRecord();
		try {
			Scanner reader = new Scanner(file);
			while (reader.hasNextLine()) {
				String line = reader.nextLine();
				if (line.startsWith("LOCUS")) {
					Locus locus = new Locus();
					locus.setLocusname(line.substring(12, 34).trim());
					locus.setSequencelength(Integer.valueOf(line.substring(35, line.indexOf(" bp ")).trim()));
					System.out.println(locus.getLocusname());
					System.out.println(locus.getSequencelength());
					gbr.setLocus(locus);
				}
				if (line.startsWith("ORIGIN")) {
					String l;
					StringBuilder sb = new StringBuilder();
					while (!(l = reader.nextLine()).trim().equals("//")) {
						l = l.substring(10,l.length()).replaceAll("\\h", "").trim();
						sb.append(l);
					}
					gbr.setOrigin(sb.toString());
				}
				if (line.startsWith("VERSION")) {
					Version version = new Version();
					version.setGenInfoID(line.substring(28).trim());
					version.setVersion(line.substring(11,24).trim());
					gbr.setVersion(version);
					System.out.println(gbr.getVersion().getVersion());
					System.out.println(gbr.getVersion().getGenInfoID());
					System.exit(0);
				}
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
