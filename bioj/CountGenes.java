package bioj;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import model.GenBankRecord;

public class CountGenes {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file = new File("/home/jannetta/workspace/BioJ/NC_012920.full.gb");
		GenBankRecord gbr = new GenBankRecord();
		int count = 0;
		try {
			Scanner reader = new Scanner(file);
			while (reader.hasNextLine()) {
				String line = reader.nextLine().trim();
				if (line.startsWith("gene")) {
					count++;
				}
			}
			System.out.println(count + " genes");
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
