package model;

public class Locus {
	private String locusname;
	private int sequencelength;
	private int molecule; //genomic DNA, genomic RNA etc
	private String genbankDiv; // genbank division
	private String modDate;
	public String getLocusname() {
		return locusname;
	}
	public void setLocusname(String locusname) {
		this.locusname = locusname;
	}
	public int getSequencelength() {
		return sequencelength;
	}
	public void setSequencelength(int sequencelength) {
		this.sequencelength = sequencelength;
	}
	public int getMolecule() {
		return molecule;
	}
	public void setMolecule(int molecule) {
		this.molecule = molecule;
	}
	public String getGenbankDiv() {
		return genbankDiv;
	}
	public void setGenbankDiv(String genbankDiv) {
		this.genbankDiv = genbankDiv;
	}
	public String getModDate() {
		return modDate;
	}
	public void setModDate(String modDate) {
		this.modDate = modDate;
	}

	
}
