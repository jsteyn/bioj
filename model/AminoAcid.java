/**
 * 
 */
package model;

import java.util.Hashtable;

import model.Mutation;
import model.MutationKey;

/**
 * @author jannetta
 *
 * Class containing information about amino acid
 */
public class AminoAcid {

	private String name;
	private int position;
	private char strand;
	private int length;
	private Hashtable<MutationKey, Mutation> mutations = new Hashtable<MutationKey, Mutation>();
	
	public AminoAcid(String name, int position, char strand, int length) {
		setName(name);
		setPosition(position);
		setStrand(strand);
		setLength(length);
	}

	public void addMutation(MutationKey mkey, Mutation mutation) {
		mutations.put(mkey, mutation);
	}
	
	public Hashtable getMutations() {
		return mutations;
	}
	
	/**
	 * Return the three letter abbreviation
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Set the three letter abbreviation
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Get the position of the amino acid sequence on the mitochondrial genome 
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * Set the position of the amino acid sequence on the mitochondrial genome
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	/**
	 * Get the strand on which this sequence is
	 * @return the strand
	 */
	public char getStrand() {
		return strand;
	}
	/**
	 * Set the strand on which this sequence is
	 * @param strand the strand to set
	 */
	public void setStrand(char strand) {
		this.strand = strand;
	}

	/**
	 * get the length of the amino acid sequence
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Set the length of the amino acid sequence
	 * @param length
	 */
	public void setLength(int length) {
		this.length = length;
	}




}
