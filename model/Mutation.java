/**
 *
 */
package model;

/**
 * @author jannetta
 */

public class Mutation {

    private int position; // position of this mutation on the mitochondrial genome
    private String locus;
    private String disease; // associated diseas
    private String allele; // reference, position and mutation: A12345T
    private String rna;
    private String homoplasmy;
    private String heteroplasmy;
    private String status;
    private Integer gb;
    private Integer references;
    private int relative_position;
    private char reference;
    private char mutation;
    private int count_positive = 0;

    public Mutation(Integer position, String locus, String disease, String allele,
                    String rna, String homoplasmy,
                    String heteroplasmy, String status, Integer gb, Integer references, int relative_position) {

        this.position = position;
        this.locus = locus;
        this.disease = disease;
        this.allele = allele;
        this.rna = rna;
        this.homoplasmy = homoplasmy;
        this.heteroplasmy = heteroplasmy;
        this.status = status;
        this.gb = gb;
        this.references = references;
        this.relative_position = relative_position;
        this.reference = allele.charAt(0);
        this.mutation = allele.charAt(allele.length()-1);
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * String
     *
     * @return the locus
     */
    public String getLocus() {
        return locus;
    }

    /**
     * @param locus the locus to set
     */
    public void setLocus(String locus) {
        this.locus = locus;
    }

    /**
     * @return the disease
     */
    public String getDisease() {
        return disease;
    }

    /**
     * @param disease the disease to set
     */
    public void setDisease(String disease) {
        this.disease = disease;
    }

    /**
     * @return the allele
     */
    public String getAllele() {
        return allele;
    }

    /**
     * @param allele the allele to set
     */
    public void setAllele(String allele) {
        this.allele = allele;
    }

    /**
     * @return the rna
     */
    public String getRna() {
        return rna;
    }

    /**
     * @param rna the rna to set
     */
    public void setRna(String rna) {
        this.rna = rna;
    }

    /**
     * @return the homoplasmy
     */
    public String getHomoplasmy() {
        return homoplasmy;
    }

    /**
     * @param homoplasmy the homoplasmy to set
     */
    public void setHomoplasmy(String homoplasmy) {
        this.homoplasmy = homoplasmy;
    }

    /**
     * @return the heteroplasmy
     */
    public String getHeteroplasmy() {
        return heteroplasmy;
    }

    /**
     * @param heteroplasmy the heteroplasmy to set
     */
    public void setHeteroplasmy(String heteroplasmy) {
        this.heteroplasmy = heteroplasmy;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the gb
     */
    public Integer getGb() {
        return gb;
    }

    /**
     * @param gb the gb to set
     */
    public void setGb(Integer gb) {
        this.gb = gb;
    }

    /**
     * @return the references
     */
    public Integer getReferences() {
        return references;
    }

    /**
     * @param references the references to set
     */
    public void setReferences(Integer references) {
        this.references = references;
    }

    /**
     * @return the relative position
     */
    public int getRelative_position() { return relative_position; }

    /**
     * @param relative_position the relative position of this mutation on the sequence
     */
    public void setRelative_position(int relative_position) {
        this.relative_position = relative_position;
    }


    public char getReference() {
        return reference;
    }

    public void setReference(char reference) {
        this.reference = reference;
    }

    public char getMutation() {
        return mutation;
    }

    public void setMutation(char mutation) {
        this.mutation = mutation;
    }

    public void incCount_positive() {
        count_positive++;
    }

    public int getCount_positive() {
        return count_positive;
    }
}
