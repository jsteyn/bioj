package model;

import java.util.ArrayList;

public class GenBankRecord {
	// http://www.ncbi.nlm.nih.gov/Sitemap/samplerecord.html
	private Locus locus; // LOCUS
	private String definition; // DEFINITION
	private String accession; // ACCESSION
	private Version version; // VERSION
	private String keywords; // KEYWORDS
	private String source; // SOURCE
	private String organism; // ORGANISM
	private ArrayList<Reference> references;
	private String comment; // COMMENT
	private ArrayList<Feature> features; // FEATURES
	private String origin; //ORIGIN
	/**
	 * @return the locus
	 */
	public Locus getLocus() {
		return locus;
	}
	/**
	 * @param locus the locus to set
	 */
	public void setLocus(Locus locus) {
		this.locus = locus;
	}
	/**
	 * @return the definition
	 */
	public String getDefinition() {
		return definition;
	}
	/**
	 * @param definition the definition to set
	 */
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	/**
	 * @return the accession
	 */
	public String getAccession() {
		return accession;
	}
	/**
	 * @param accession the accession to set
	 */
	public void setAccession(String accession) {
		this.accession = accession;
	}
	/**
	 * @return the version
	 */
	public Version getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(Version version) {
		this.version = version;
	}
	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}
	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the organism
	 */
	public String getOrganism() {
		return organism;
	}
	/**
	 * @param organism the organism to set
	 */
	public void setOrganism(String organism) {
		this.organism = organism;
	}
	/**
	 * @return the references
	 */
	public ArrayList<Reference> getReferences() {
		return references;
	}
	/**
	 * @param references the references to set
	 */
	public void setReferences(ArrayList<Reference> references) {
		this.references = references;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the features
	 */
	public ArrayList<Feature> getFeatures() {
		return features;
	}
	/**
	 * @param features the features to set
	 */
	public void setFeatures(ArrayList<Feature> features) {
		this.features = features;
	}
	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}
	/**
	 * @param origin the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	
	
	
}
