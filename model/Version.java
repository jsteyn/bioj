package model;

public class Version {
	private String version;
	private String genInfoID; // DBLINK, GI-GenInfo Identifier
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the genInfoID
	 */
	public String getGenInfoID() {
		return genInfoID;
	}
	/**
	 * @param genInfoID the genInfoID to set
	 */
	public void setGenInfoID(String genInfoID) {
		this.genInfoID = genInfoID;
	}

	
}
