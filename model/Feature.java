package model;

public class Feature {
	private String source; // length, organism, organell, mol_type, db_xref
	private String taxon; // join, gene, locus_tag, db_xref
	private String cds; // CDS
}
