/**
 * 
 */
package model;

/**
 * @author jannetta
 *
 */
public class MutationKey {

	private int position;
	private String allele;
	
	public MutationKey(int position, String allele) {
		this.position = position;
		this.allele = allele;
	}
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	/**
	 * @return the rna
	 */
	public String getRna() {
		return allele;
	}
	/**
	 * @param rna the rna to set
	 */
	public void setRna(String rna) {
		this.allele = rna;
	}

	
}
