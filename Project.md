**PI:** Jo Elson

**Other:** Rache Queen

**Research:** Pathogenic mitochondrial tRNAs

Pipeline
========

* Input: Per species directories containing per tRNA files for each amino acid, including human tRNA as first line, in fasta format
* Run mafft on each file with --preservecase switch
* Extract human mithondrial genome from GenBank: NC_012920.1.fasta
* Download know rna mutations from MITOMAP
* create strand.txt file containing: AminoAcid startposition strand length (tab separated)

Notes:
======
* Remove quotes from mutations files
* NO SPACES IN FILENAMES
* Maintain amino acid abbreviations throughout, i.e. in files and as filenames, eg Leu (UUR) becomes Leu_UUR. See strand.txt file
